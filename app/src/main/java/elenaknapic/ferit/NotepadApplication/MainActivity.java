package elenaknapic.ferit.NotepadApplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.Toolbar;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    NoteAPI noteAPI;

    //recycler
    private RecyclerView mRecyclerView;
    private NoteAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Note> noteList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //initialize
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new NoteAdapter(noteList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {

            @Override
            public void onUpdateClick(int position) {
                updateNote(position);
            }

            @Override
            public void onDeleteClick(int position) {
                deleteNote(position);
            }

            @Override
            public void onItemClick(int position) {
                Toast.makeText(MainActivity.this, "Whole item clicked at position: " + position, Toast.LENGTH_SHORT).show();
            }
        });


        addNote();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.add){
            Intent intent = new Intent(this, AddNoteActivity.class);
            startActivity(intent);
            Toast.makeText(MainActivity.this, "Add button is clicked" , Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }



    public void addNote(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.SERVER_IP)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NoteAPI noteAPI = retrofit.create(NoteAPI.class);

        Call<List<Note>> call = noteAPI.getAllNotes();

        call.enqueue(new Callback<List<Note>>() {
            @Override
            public void onResponse(Call<List<Note>> call, Response<List<Note>> response) {
                if (!response.isSuccessful()) {
                    return;
                }

                List<Note> notes = response.body();
                noteList.clear();

                for (Note note : notes) {


                    noteList.add(new Note(note.getId(), note.getTitle(), note.getNote()));

                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Note>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void deleteNote(int position) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.SERVER_IP)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NoteAPI noteAPI = retrofit.create(NoteAPI.class);

        int id = noteList.get(position).getId();
        Call<Void> call = noteAPI.deleteNote(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(getApplicationContext(), "Successfully deleted", Toast.LENGTH_LONG).show();
                addNote();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
        public void updateNote(int position) {
            Intent intent = new Intent(this, OpenNoteActivity.class);
            intent.putExtra("title", noteList.get(position).getTitle());
            intent.putExtra("note", noteList.get(position).getNote());
            startActivity(intent);

        }



    @Override
    protected void onResume() {
        super.onResume();
        addNote();
    }



}