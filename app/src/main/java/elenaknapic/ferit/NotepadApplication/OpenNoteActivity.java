package elenaknapic.ferit.NotepadApplication;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class OpenNoteActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open);

        TextView openTitle = findViewById(R.id.openTitle);
        TextView openNote= findViewById(R.id.openNote);

        String title = "Title not set";
        String note = "Note not set";
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            title = extras.getString("title");
            note = extras.getString("note");
        }


        openTitle.setText(title);
        openNote.setText(note);


    }
}
