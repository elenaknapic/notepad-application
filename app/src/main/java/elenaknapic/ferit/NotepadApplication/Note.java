package elenaknapic.ferit.NotepadApplication;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Note {
    @Expose
    @SerializedName("id")
    int id;

    @Expose
    @SerializedName("note")
    String note;

    @Expose
    @SerializedName("title")
    String title;


    public Note(int id, String note, String title) {
        this.id = id;
        this.note = note;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
