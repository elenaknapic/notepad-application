package elenaknapic.ferit.NotepadApplication;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNoteActivity extends AppCompatActivity {
    NoteAPI noteAPI;

    private EditText addNoteTitle;
    private EditText addNoteText;

    Button btn_add_note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        addNoteTitle = findViewById(R.id.addNoteTitle);
        addNoteText = findViewById(R.id.addNoteText);

        noteAPI = RetrofitClientInstance.getRetrofitInstance().create(NoteAPI.class);

        btn_add_note = findViewById(R.id.btn_add_note);
        btn_add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertNote();
            }
        });
    }

    private void insertNote() {
        String note_title = addNoteTitle.getText().toString();
        String note_text = addNoteText.getText().toString();

        Call<Void> call = noteAPI.insertNote(note_title, note_text);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                else{
                    Toast.makeText(AddNoteActivity.this, "Note added", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(AddNoteActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
