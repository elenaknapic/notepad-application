package elenaknapic.ferit.NotepadApplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NoteAPI {
    //dohvaca biljeske za recycler view
    @GET("/notes")
    Call<List<Note>> getAllNotes();


    //dodaje zadatak
    @FormUrlEncoded
    @POST("/unos")
     Call<Void> insertNote(@Field("title") String title,
                                 @Field("note") String note
    );

    @FormUrlEncoded
    @POST("/deleteNote")
    Call<Void> deleteNote(@Field("id") int id);




}
